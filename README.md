# Sonarqube Local Server

## Prerequisites

- Docker

## What's sonarqube?

It’s a code quality management platform that allows developer teams to manage, track and eventually improve the quality of the source code.

It’s a web based application that keeps historical data of a variety of metrics and gives trends of leading and lagging indicators for all seven deadly sins of developers.


### Supported languages

- Java
- C#
- VB.NET
- JS
- TS
- Go
- Python
- PHP

## Why do I need this?

### **Increase Developer Skills**

The development teams as a part of their development process can adopt it quickly, because SonarQube provides enormous value to the development teams.

The development teams receive regular feedbacks on quality issues and it helps them increase their programming skills.

SonarQube helps developers to understand the quality of their software and ensures the transparency of code.

### **Scale With Business Needs**

SonarQube is designed to scale with business needs. There has been no limit discovered to its scalability yet.

SonarQube has been tested in environments. It performs daily analysis on more than five thousand projects with more than four million lines of code and twenty developers.

### **Sonar covers the 7 sections of code quality**

1. Architecture and Design
2. Unit tests
3. Duplicated code
4. Potential bugs
5. Complex code
6. Coding standards
7. Comments

SonarQube receives files as an input and analyzes them along with barriers. Then calculates a set of metrics, stores them in a database and shows them on a dashboard. This recursive implementation helps in analysis of code quality and how code improves over time.


## How to run this?

Just clone the repo and type the `docker-compose` command:

  ```shell
  $ docker-compose up
  ```

Now the sonarqube is available on `localhost:9000` the default credentials are:

```
Username: admin
Password: admin
```

**Note:** You need to install a plugin on your project ([Plugin list](https://docs.sonarqube.org/display/PLUG/)).

### Example to run on maven project

Execute on your maven project the command:

```shell
$ mvn sonar:sonar -Dsonar.host.url=http://localhost:9000 -Dsonar.login=852d6622683aa2c0836814e26bf19573c9a9cfa5 
```

## Future updates

- Integration with the jdk 11 (already exists but have some issues)